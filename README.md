# Polarion Workflow Function ExecuteLinkedWorkItemsAction

The latest jar file is available in the [releases section](../../releases).

## Introduction

Polarion offers the built-in work item workflow function `LinkedWorkItemsStatusChange` that can change the status of linked work items. However, this workflow function directly changes the work item status and does not perform the workflow actions. Consequently, configured workflow conditions are not checked and workflow functions not executed.

In contrast to that, the `ExecuteLinkedWorkItemsAction` work item workflow function lets you configure the workflow actions of linked work items that shall be executed on a status change.

The parameters are:

| Parameter | Description |
| ---      | ---      |
| action.id | The ID of the action that shall be performed for the status change. If the action is generally available from the origin to the target status, but blocked due to some conditions, a UserFriendlyException is thrown and the action canceled. |
| link.roles.direct | Comma-separated list of outgoing link roles to check. Either direct or back needs to be specified. |
| link.roles.back | Comma-separated list of incoming link roles to check. Either direct or back needs to be specified. |
| origin.states | Comma-separated list of origin states of the linked work items for which the action shall be performed. |
| origin.negate | Optional: If the parameter is set to true, the origin.states are not included but excluded. (default: false) |
| target.state | The single target state to which the linked work item shall transition. |
| workitem.type.ids | Optional: Comma-separated list of work item type ids. The action shall only be executed for these types. |
| follow.links | Optional: If the parameter is set to true, the workflow action is not only executed for directly linked work items but also for their linked work items and so on. (default: false) Linked work item loops are caught and result in a UserFriendlyException and the action is canceled. |



## License

Download and use of the extension is free. By downloading the extension, you accept the license terms.

See [NOTICE.md](NOTICE.md) for licensing details.


## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for contribution guidelines.


## Download & Installation

### Installation:

1. Go to the [releases section](../../releases) of this repository and download the latest version of the extension.

1. Copy the root folder `com.tulrfsd.polarion.workflow.functions.executelinkedworkitemsaction` in the extensions folder of your Polarion server.

1. Restart your Polarion server.

### Uninstall:

- Remove the folder `com.tulrfsd.polarion.workflow.functions.executelinkedworkitemsaction` from the extensions folder of your Polarion installation and restart the Polarion server.

### Compatibility:

- Compiled against Polarion 20R1
- Tested with Polarion 22R2
